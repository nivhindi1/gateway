import { GraphQLExtension } from 'apollo-server';
import { GraphQLContext } from '../api/graphql-context';
import { LogMessage } from '@warrior/winston-logger-utils';
import { ApolloError } from 'apollo-server';

export class AdditionalGraphqlExtension implements GraphQLExtension {
    requestDidStart(o) {
        if (o.operationName != 'IntrospectionQuery') {
            const loggerDefaultMeta: LogMessage = {
                scope: 'graphql-request'
            };
            const logger = (o.context as GraphQLContext).logger.child(loggerDefaultMeta);

            logger.debug(`graphql request body: ${o.queryString}`, { queryString: o.queryString });
            logger.info(`incoming graphql request - upn=${o.context.req.headers.oidc_claim_upn}`, {
                upn: o.context.req.headers.oidc_claim_upn,
                operationName: o.operationName
            });
        }
    }

    willSendResponse(o) {
        if (o.context.req.body.operationName != 'IntrospectionQuery') {
            const loggerDefaultMeta: LogMessage = {
                scope: 'graphql-request'
            };
            const logger = (o.context as GraphQLContext).logger.child(loggerDefaultMeta);

            if (o.graphqlResponse.errors) {
                o.graphqlResponse.errors.forEach(error => {
                    logger.error(JSON.stringify(error.message));
                    throw new ApolloError(JSON.stringify(error.message));
                });
            }

            const tracing = o.graphqlResponse.extensions.tracing;
            logger.info(
                `finished graphql request, took ${tracing.duration / 1000000}ms upn=${
                    o.context.req.headers.oidc_claim_upn
                }`,
                {
                    upn: o.context.req.headers.oidc_claim_upn,
                    operationName: o.context.req.body.operationName
                }
            );
            logger.debug(`graphql tracing info graphqlTracing=${JSON.stringify(tracing, null, '\t')}`, {
                graphqlTracing: JSON.stringify(tracing, null, '\t')
            });
        }
    }
}
