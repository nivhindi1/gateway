import * as warriorWinstonLogger from '@warrior/winston-logger-utils';

import { Environment } from './environment';

const defaultMeta: warriorWinstonLogger.LogMessage = {
	system_ID: Environment.getConfig().metadata.system_ID,
	system_name: Environment.getConfig().metadata.system_name,
	component: Environment.getConfig().metadata.component,
	environment: Environment.getName()
};

export const rootLogger = warriorWinstonLogger.createLogger(Environment.getConfig().logger, defaultMeta);

/**
 * Winston logger stream for the morgan plugin
 */
export const winstonStream = warriorWinstonLogger.createWinstonStream(rootLogger);
