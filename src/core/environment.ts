import { Configuration, default as config } from "../config";

export class Environment {
    static conf: Configuration;

    static getName(): string {
        return process.env.NODE_ENV || "dev";
    }

    static isTest(): boolean {
        return this.getName() === "test";
    }

    static isDevelopment(): boolean {
        return this.getName() === "dev";
    }

    static isProduction(): boolean {
        return this.getName() === "prod";
    }

    static loadConfig() {
        if (this.isTest() || this.isDevelopment()) {
            this.conf = config[this.getName()];
        } else {
            let osConfig = require(`/app/dist/config/${this.getName()}.json`);
            let envConf = config[this.getName()];
            this.joinConfigs(osConfig, envConf);
            this.conf = envConf;
        }
    }

    static joinConfigs(osConfig: Object, envConf: Object) {
        Object.keys(osConfig).forEach(key => {
            if (typeof envConf[key] === "object") {
                this.joinConfigs(osConfig[key], envConf[key]);
            } else {
                envConf[key] = osConfig[key];
            }
        });
    }

    static getConfig(): Configuration {
        return this.conf;
    }
}

Environment.loadConfig();
