import express from 'express';
import * as http from 'http';
import { ApolloServer } from 'apollo-server-express';
import { rootLogger } from '../core';
import { Environment } from '.';
import { GraphqlContextFactory } from '../api/graphql-context';
import { AdditionalGraphqlExtension } from './additional-graphql-extension';
import { stitchedSchema } from '../api/stitched-schema';
import { LogMessage } from '@warrior/winston-logger-utils';
const loggerDefaultMeta: LogMessage = {
    scope: 'app:core:server'
};
const logger = rootLogger.child(loggerDefaultMeta);

export class Server {
    static init(): express.Application {
        return express();
    }

    static async run(app: express.Application, port: string): Promise<http.Server> {
        await this.connectApolloServer(app);

        const server = app.listen(this.normalizePort(port));
        server.on('listening', () => this.onListening(server));
        server.on('error', error => this.onError(server, error));
        logger.info(`Server was started on environment ${Environment.getName()}`);

        return server;
    }

    private static async connectApolloServer(app: express.Application): Promise<void> {
        const schema = await stitchedSchema.getSchema();

        const apolloServer = new ApolloServer({
            uploads: false,
            schema,
            context: ({ req, res }) => {
                return GraphqlContextFactory.createContext({ req, res });
            },
            tracing: true,
            extensions: [() => new AdditionalGraphqlExtension()]
        });
        apolloServer.applyMiddleware({ app, path: '/graphql' });

        stitchedSchema.outputSchema$.subscribe(async newSchemaPromise => {
            const newSchema = await newSchemaPromise;

            logger.info('updated graphql schema');
            (apolloServer as any).schema = newSchema;
        });
    }

    private static normalizePort(port: string): number | string | boolean {
        const parsedPort = parseInt(port, 10);
        if (isNaN(parsedPort)) {
            // named pipe
            return port;
        }
        if (parsedPort >= 0) {
            // port number
            return parsedPort;
        }
        return false;
    }

    private static onListening(server: http.Server): void {
        logger.info(`Listening on ${this.bind(server.address())}`);
    }

    private static onError(server: http.Server, error: Error): void {
        if (error['syscall'] !== 'listen') {
            throw error;
        }
        const addr = server.address();
        // handle specific listen errors with friendly messages
        switch (error['code']) {
            case 'EACCES':
                logger.error(`port ${this.bind(addr)} requires elevated privileges`);
                this.exit(1);
                break;
            case 'EADDRINUSE':
                logger.error(`port ${this.bind(addr)} is already in use`);
                this.exit(1);
                break;
            default:
                throw error;
        }
    }

    private static exit(code) {
        rootLogger.on('finish', () => {
            setTimeout(() => process.exit(code), 2000);
        });
        rootLogger.end();
    }

    private static bind(addr: string | any): string {
        return addr ? (typeof addr === 'string' ? `pipe ${addr}` : `port ${addr.port}`) : 'unknown';
    }
}
