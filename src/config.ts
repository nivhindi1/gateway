import { MicroServiceConstants } from './consts';

/**
 * common configuration that can be overriden per environment
 */
const commonConfig = {
    metadata: {
        system_ID: '26',
        system_name: '', // deleted
        component: '' // deleted
    },
    server: {
        port: process.env.PORT || '8080',
        graphiql: true
    },
    logger: {
        console: {
            level: process.env.LOGGER_LEVEL || 'info'
        },
        logstash: {
            level: process.env.LOGSTASH_LEVEL || 'info',
            host: process.env.LOGSTASH_HOST || 'localhost',
            port: process.env.LOGSTASH_PORT || '4200',
            protocol: process.env.LOGSTASH_PROTOCOL || 'udp4'
        }
    },
    schemaStitching: {
        pollRate: 60000,
        timeout: 5000,
        services: [
            {
                endpoint:  process.env.MS_OC_ENDPOINT,
                logicalName: MicroServiceConstants.OC_SERVICE
            },
            {
                endpoint:  process.env.MS_TA_ENDPOINT,
                logicalName: MicroServiceConstants.TA_SERVICE
            },
            {
                endpoint: process.env.MS_TAR_ENDPOINT,
                logicalName: MicroServiceConstants.TAR_SERVICE
            },
            {
                endpoint: process.env.MS_USER_ENDPOINT,
                logicalName: MicroServiceConstants.USER_SERVICE
            },
            {
                endpoint: process.env.MS_RE_ENDPOINT,
                logicalName: MicroServiceConstants.RE_SERVICE
            },
            {
                endpoint: process.env.MS_LAYERS_ENDPOINT,
                logicalName: MicroServiceConstants.LAYERS_SERVICE
            },
            {
                endpoint: process.env.MS_PL_ENDPOINT,
                logicalName: MicroServiceConstants.PL_SERVICE
            },
            {
                endpoint: process.env.MS_DEMAND_ENDPOINT,
                logicalName: MicroServiceConstants.DEMAND_SERVICE
            }
        ]
    }
} as Configuration;

const configuration = {
    /**
     * Development Environment
     * ------------------------------------------
     *
     * This is the local development environment, which is used by the developers
     */
    dev: {
        ...commonConfig,
        logger: {
            ...commonConfig.logger,
            logstash: {
                level: 'info',
                host: 'elk-dev-lga-1.mamdas.iaf',
                port: '64686',
                protocol: 'udp4'
            }
        },
        schemaStitching: {
            ...commonConfig.schemaStitching,
            services: [
                {
                    endpoint: process.env.MS_OC_ENDPOINT || 'http://localhost:8086/graphql',
                    logicalName: MicroServiceConstants.OC_SERVICE
                },
                {
                    endpoint: process.env.MS_TA_ENDPOINT || 'http://localhost:8088/graphql',
                    logicalName: MicroServiceConstants.TA_SERVICE
                },
                {
                    endpoint: process.env.MS_TAR_ENDPOINT || 'http://localhost:8090/graphql',
                    logicalName: MicroServiceConstants.TAR_SERVICE
                },
                {
                    endpoint: process.env.MS_USER_ENDPOINT || 'http://localhost:8083/graphql',
                    logicalName: MicroServiceConstants.USER_SERVICE
                },
                {
                    endpoint: process.env.MS_RE_ENDPOINT || 'http://localhost:8099/graphql',
                    logicalName: MicroServiceConstants.RE_SERVICE
                },
                {
                    endpoint: process.env.MS_LAYERS_ENDPOINT || 'http://localhost:8092/graphql',
                    logicalName: MicroServiceConstants.LAYERS_SERVICE
                },
                {
                    endpoint: process.env.MS_PL_ENDPOINT || 'http://localhost:8094/graphql',
                    logicalName: MicroServiceConstants.PL_SERVICE
                },
                {
                    endpoint: process.env.MS_DEMAND_ENDPOINT || 'http://localhost:8096/graphql',
                    logicalName: MicroServiceConstants.DEMAND_SERVICE
                }
            ]
        },
        userService: {
            endpoint: 'http://localhost:8083/graphql'
        }
    },
    /**
     * Test Environment
     * ------------------------------------------
     *
     * This environment is used by the unit, migration and database test.
     */
    test: {
        ...commonConfig,
        logger: {
            ...commonConfig.logger,
            logstash: {
                level: process.env.LOGSTASH_LEVEL || 'info',
                host: process.env.LOGSTASH_HOST || 'elk-dev-lga-1.mamdas.iaf',
                port: process.env.LOGSTASH_PORT || '64686',
                protocol: process.env.LOGSTASH_PROTOCOL || 'udp4'
            }
        }
    },
    /**
     * Test Environment
     * ------------------------------------------
     *
     * This environment is used by the unit, migration and database test.
     */
    int: {
        ...commonConfig,
        logstash: {
            host: 'elk-dev-lga-1.mamdas.iaf',
            port: '64686'
        },
        schemaStitching: {
            ...commonConfig.schemaStitching,
            services: [
                {
                    endpoint: 'http://something:8086/graphql',
                    logicalName: MicroServiceConstants.OC_SERVICE
                },
                {
                    endpoint: 'http://something:8088/graphql',
                    logicalName: MicroServiceConstants.TA_SERVICE
                },
                {
                    endpoint: 'http://something:8090/graphql',
                    logicalName: MicroServiceConstants.TAR_SERVICE
                },
                {
                    endpoint: 'http://something:8083/graphql',
                    logicalName: MicroServiceConstants.USER_SERVICE
                },
                {
                    endpoint: 'http://something:8099/graphql',
                    logicalName: MicroServiceConstants.RE_SERVICE
                },
                {
                    endpoint: 'http://something:8094/graphql',
                    logicalName: MicroServiceConstants.PL_SERVICE
                },
                {
                    endpoint: 'http://something:8092/graphql',
                    logicalName: MicroServiceConstants.LAYERS_SERVICE
                },
                {
                    endpoint: 'http://something:8096/graphql',
                    logicalName: MicroServiceConstants.DEMAND_SERVICE
                }
            ]
        },
        userService: {
            endpoint: 'http://user-service.warrior.svc:8083/graphql'
        }
    },
    /**
     * Production Environment
     * ------------------------------------------
     *
     * This configuration will be used by the cloud servers. You are abel to override
     * them with the local cloud environment variable to make it even more configurable.
     */
    prod: {
        ...commonConfig,
        server: {
            ...commonConfig.server,
            graphiql: false
        },
        logger: {
            ...commonConfig.logger,
            logstash: {
                ...commonConfig.logger.logstash,
                host: 'something',
                port: '64686',
            }
        },
        schemaStitching: {
            ...commonConfig.schemaStitching,
            services: [
                {
                    endpoint: "http://something:8086/graphql",
                    logicalName: MicroServiceConstants.OC_SERVICE
                },
                {
                    endpoint: "http://something:8088/graphql",
                    logicalName: MicroServiceConstants.TA_SERVICE
                },
                {
                    endpoint: "http://something:8090/graphql",
                    logicalName: MicroServiceConstants.TAR_SERVICE
                },
                {
                    endpoint: "http://something:8083/graphql",
                    logicalName: MicroServiceConstants.USER_SERVICE
                },
                {
                    endpoint: "http://something:8099/graphql",
                    logicalName: MicroServiceConstants.RE_SERVICE
                },
                {
                    endpoint: 'http://something:8094/graphql',
                    logicalName: MicroServiceConstants.PL_SERVICE
                },
                {
                    endpoint: 'http://something:8092/graphql',
                    logicalName: MicroServiceConstants.LAYERS_SERVICE
                },
                {
                    endpoint: 'http://something:8096/graphql',
                    logicalName: MicroServiceConstants.DEMAND_SERVICE
                }
            ]
        }
    }
} as Environments;
export default configuration;

export interface Environments {
    dev: Configuration;
    test: Configuration;
    prod: Configuration;
}

export interface Configuration {
    metadata: {
        system_ID: string;
        system_name: string;
        component: string;
    };

    server: {
        port: string;
        graphiql: boolean;
    };
    logger: {
        console: {
            level: string;
        };
        logstash?: {
            level: string;
            host: string;
            port: string;
            protocol: string;
        };
    };
    schemaStitching: {
        pollRate: number;
        timeout: number;
        services: MicroServiceConf[];
    };
    userService: {
        endpoint: string;
    };
}

export interface MicroServiceConf {
    endpoint: string;
    logicalName: string;
}
