import helmet from 'helmet';
import cors from 'cors';
import * as uuid from 'uuid';
import * as morgan from 'morgan';
import { setUpnInHeaders, checkWarriorTeam } from './middleware';

// Core elements to get the server started
import { Environment, Server } from './core';

// Import all routes
import routes from './endpoints';

Environment.loadConfig();

// Create a new express app
export const app = Server.init();

// Helmet helps you secure your Express apps by setting various HTTP headers
app.use(helmet());
app.use(helmet.noCache());
app.use(
    helmet.hsts({
        maxAge: 31536000,
        includeSubdomains: true
    })
);

// Enable cors for all routes and origins
app.use(cors());

// Set req id
app.use((req, res, next) => {
    res.locals = {
        requestId: uuid.v4()
    };

    req.headers.message_id = res.locals.requestId;

    next();
});

morgan.token('req-id', (req, res) => {
    return res.locals.requestId;
});

app.use(setUpnInHeaders);
app.use(checkWarriorTeam);

morgan.token('upn', (req, res) => {
    console.log(req.headers);
    return req.headers.oidc_claim_upn;
});

// Map routes to the express application
app.use(routes);

// Starts the server and listens for common errors
export default Server.run(app, Environment.getConfig().server.port);
