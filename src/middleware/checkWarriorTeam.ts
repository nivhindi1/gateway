import { Request, Response, NextFunction } from 'express';
import { GraphQLClient } from 'graphql-request';
import { Environment } from '../core/environment';
import { LogMessage } from '@warrior/winston-logger-utils';
import { rootLogger } from '../core';

const loggerDefaultMeta: LogMessage = {
    scope: 'check-warrior-team'
};

const logger = rootLogger.child(loggerDefaultMeta);

export async function checkWarriorTeam(req: Request, res: Response, next: NextFunction) {
    if (Environment.getName() === 'dev' && !req.headers.teamid) {
        next();
    } else {
        let graphqlClient = new GraphQLClient(Environment.getConfig().userService.endpoint, {
            headers: {
                'reality-id': '0',
                oidc_claim_upn: req.headers.oidc_claim_upn.toString()
            }
        });

        const query = `{
            userInfo {
                upn,
                warriorTeams {
                id
                }
            }
        }`;

        const userInfo: any = await graphqlClient.request(query);
        userInfo;

        if (!(userInfo.userInfo.warriorTeams.filter(x => x.id === req.headers.teamid).length > 0)) {
            logger.error('Permission error! This user is not permitted to this warrior team');
        }

        next();
    }
}
