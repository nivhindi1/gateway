import { Request, Response, NextFunction } from 'express';

export function setUpnInHeaders(req: Request, res: Response, next: NextFunction) {
    if (!req.headers.oidc_claim_upn) {
        req.headers.oidc_claim_upn = "warrior@mamdas.iaf";
    }

    next();
}