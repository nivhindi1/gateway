import { Router } from 'express';
import graphiqlEndpoint from './graphiql';

const router = Router();
router.use(graphiqlEndpoint);

export default router;
