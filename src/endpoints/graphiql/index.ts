import { Router, static as expressStatic } from 'express';
import { Environment } from '../../core/environment';
import { graphiqlExpress } from './graphiQLCostume';

const router = Router();
if (Environment.getConfig().server.graphiql) {
	// kind of shitty solution so graphiql will work offline with subscriptions :(
	router.use('/graphiql-resources', expressStatic(__dirname + '/resources'));

	router.use('/graphiql', graphiqlExpress({
		endpointURL: '/graphql',
	}));
}

export default router;
