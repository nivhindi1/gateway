import { makeRemoteExecutableSchema, introspectSchema } from 'graphql-tools';
import { GraphQLSchema } from 'graphql';
import * as fetch from 'node-fetch';
import { HttpLink } from 'apollo-link-http';
import { ApolloLink } from 'apollo-link';
import { setContext } from 'apollo-link-context';

import { Environment } from '../core/environment';
import { rootLogger } from '../core';
import { GraphQLContext } from './graphql-context';
import { LogMessage } from '@warrior/winston-logger-utils';

const loggerDefaultMeta: LogMessage = {
    scope: 'app:remote-schemas'
};

const logger = rootLogger.child(loggerDefaultMeta);

class RemoteSchemas {
    public async retreiveSchemas(): Promise<Map<string, GraphQLSchema>> {
        const schemas = new Map<string, GraphQLSchema>();

        for (const serviceConfig of Environment.getConfig().schemaStitching.services) {
            // introspection should use a link with a timeout to not postpone requests
            const introspectLink = new HttpLink({
                uri: serviceConfig.endpoint,
                fetch,
                fetchOptions: {
                    timeout: Environment.getConfig().schemaStitching.timeout
                }
            });

            try {
                const remoteSchema = await introspectSchema(introspectLink);
                const remoteExecutableSchema = makeRemoteExecutableSchema({
                    schema: remoteSchema,
                    link: this.getRemoteSchemaLink(serviceConfig)
                });

                schemas.set(serviceConfig.logicalName, remoteExecutableSchema);
            } catch (err) {
                logger.error(`failed to get ${serviceConfig.logicalName} (${serviceConfig.endpoint}) graphql schema`, {
                    error: err
                });
            }
        }

        return schemas;
    }

    public getRemoteSchemaLink(serviceConfig: { endpoint: string }): ApolloLink {
        const authLink = this.getAuthLink();
        const httpLink = new HttpLink({
            uri: serviceConfig.endpoint,
            fetch
        });
        return authLink.concat(httpLink);
    }

    public getAuthLink(): ApolloLink {
        return setContext((request, prevContext) => {
            return {
                headers: {
                    // only present so introspection queries will pass on the services
                    message_id: prevContext.graphqlContext.req.headers.message_id,
                    oidc_claim_upn: prevContext.graphqlContext.oidc_claim_upn,
                    reality_id: prevContext.graphqlContext.reality,
                    team_id: prevContext.graphqlContext.team_id,
                    ...(prevContext.graphqlContext as GraphQLContext)
                }
            };
        });
    }
}

export const remoteSchemas = new RemoteSchemas();
