import { Request, Response } from 'express';
import { Logger } from 'winston';
import { rootLogger } from '../core';
import { LogMessage } from '@warrior/winston-logger-utils';
export interface Connectors {}

export interface GraphQLContext {
    connectors: Connectors;
    logger: Logger;
    req: Request;
    reality: string;
    team_id: string;
    oidc_claim_upn: string;
}

export class GraphqlContextFactory {
    public static createContext({ req, res }: { req: Request; res: Response }): GraphQLContext {
        const realityID = req.headers.realityid ? req.headers.realityid.toString() : undefined;
        const teamID = req.headers.teamid ? req.headers.teamid.toString() : undefined;
        const oidc_claim_upn = req.headers.oidc_claim_upn ? req.headers.oidc_claim_upn.toString() : undefined;

        const contextLoggerDefaultMeta: LogMessage = {
            message_ID: res.locals ? res.locals.requestId : '',
            realityID: realityID,
            teamID: teamID
        };
        const contextLogger = rootLogger.child(contextLoggerDefaultMeta);

        return {
            connectors: {},
            req,
            logger: contextLogger,
            reality: realityID,
            team_id: teamID,
            oidc_claim_upn: oidc_claim_upn
        };
    }
}
