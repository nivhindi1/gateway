import { GraphQLSchema } from 'graphql';
import { merge } from 'lodash';
import { IResolversParameter } from 'graphql-tools';
import { rootLogger } from '../../core';

import { resolvers as testResolvers, schema as testLinkingsSchema } from './entityStitching/entityStitching.schema';
import { MicroServiceConstants } from '../../consts';
import { LogMessage } from '@warrior/winston-logger-utils';

const loggerDefaultMeta: LogMessage = {
	scope: 'app:schema-linking'
};
const logger = rootLogger.child(loggerDefaultMeta);

export interface LinkingSchema {
	schemas: string[];
	resolvers: (remoteSchemas: Map<string, GraphQLSchema>) => IResolversParameter;
}
const servicesToLinkingMap = new Map<MicroServiceConstants[], LinkingSchema>();

servicesToLinkingMap.set([ MicroServiceConstants.PLANNINGS_SERVICE, MicroServiceConstants.PRIORITIES_SERVICE ], {
	schemas: testLinkingsSchema,
	resolvers: testResolvers
});

export function relevantLinkingSchemas(remoteSchemas: Map<string, GraphQLSchema>) {
	const schemas = [];

	for (const [ relatedServices, linkingSchema ] of servicesToLinkingMap.entries()) {
		if (hasServices(remoteSchemas, relatedServices)) {
			logger.info(`adding ${getServicesNamesPretty(relatedServices)} schema linkings SDL`);
			schemas.push(...linkingSchema.schemas);
		}
	}

	return schemas;
}

export function relevantLinkingResolvers(remoteSchemas: Map<string, GraphQLSchema>) {
	let resolvers = {};

	for (const [ relatedServices, linkingSchema ] of servicesToLinkingMap.entries()) {
		if (hasServices(remoteSchemas, relatedServices)) {
			logger.info(`adding ${getServicesNamesPretty(relatedServices)} schema linkings resolvers`);
			resolvers = merge(resolvers, linkingSchema.resolvers(remoteSchemas));
		}
	}

	return resolvers;
}

function hasServices(remoteSchemas: Map<string, GraphQLSchema>, services: MicroServiceConstants[]): boolean {
	let containsAllServices = true;

	for (const service of services) {
		if (!remoteSchemas.has(service)) containsAllServices = false;
	}
	return containsAllServices;
}

function getServicesNamesPretty(services: MicroServiceConstants[]): string {
	return services.reduce((prev, curr) => {
		if (prev !== '') return `${prev} -> ${curr}`;
		else return curr;
	}, '');
}
