import { readFileSync } from 'fs';
import { IResolversParameter } from 'graphql-tools';
import { GraphQLSchema } from 'graphql';

export const schema = [readFileSync(`${__dirname}/entityStitching.linking.gql`, 'utf8')];

export const resolvers = (remoteSchemas: Map<string, GraphQLSchema>) =>  ({
	Priority: {
		planing: {
			fragment: `... on Priority { __typename }`,
			resolve(root, args, context, info) {
				const id = "123";
				return info.mergeInfo.delegateToSchema({
					schema: remoteSchemas.get("plannings-service"),
					operation: 'query',
					fieldName: 'plannings',
					args: {
                        id: id
					},
					context,
					info,
				});
			},
		},
	},
} as IResolversParameter);
