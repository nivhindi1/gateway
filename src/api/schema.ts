import { merge } from 'lodash';
import { makeExecutableSchema } from 'graphql-tools';
import { resolvers as rootResolvers, typeDef as rootDefs } from './root/root.schema';
import { resolvers as scalarsResolvers, typeDefs as scalarsDefs } from './scalars';
import { rootLogger } from '../core';
export const typeDefs = [ rootDefs, ...scalarsDefs ];

export const resolvers = merge(rootResolvers, scalarsResolvers);

export const executableSchema = makeExecutableSchema({
	typeDefs,
	resolvers,
	logger: { log: (e) => rootLogger.error(e.toString(), { error: e }) }
});

export default executableSchema;
