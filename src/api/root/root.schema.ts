import { gql, IResolvers } from 'apollo-server';
import { GraphQLContext } from '../graphql-context';

export const typeDef = gql`
type Query {
    _blank: String
}

type Mutation {
    _blank: String
}

type IrrelevantEntities {
    _blank: String
}
`;

export const resolvers = {
} as IResolvers<any, GraphQLContext>;
