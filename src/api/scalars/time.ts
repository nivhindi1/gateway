import { gql } from 'apollo-server';
import { GraphQLTime } from 'graphql-iso-date';

export const typeDef = gql`
scalar Time
`;

export const timeResolver = {
	Time: GraphQLTime,
};
