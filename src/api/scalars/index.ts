import { merge } from 'lodash';

import { typeDef as dateDef, dateResolver } from './date';
import { typeDef as timeDef, timeResolver } from './time';
import { typeDef as dateTimeDef, dateTimeResolver } from './datetime';

export const typeDefs = [dateDef, timeDef, dateTimeDef];

export const resolvers = merge(dateResolver, timeResolver, dateTimeResolver);
