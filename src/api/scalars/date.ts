import { gql } from 'apollo-server';
import { GraphQLDate } from 'graphql-iso-date';

export const typeDef = gql`
scalar Date
`;

export const dateResolver = {
	Date: GraphQLDate,
};
