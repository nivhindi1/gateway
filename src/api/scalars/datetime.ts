import { gql } from 'apollo-server';
import { GraphQLDateTime } from 'graphql-iso-date';

export const typeDef = gql`
scalar DateTime
`;

export const dateTimeResolver = {
	DateTime: GraphQLDateTime,
};
