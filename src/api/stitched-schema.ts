import { mergeSchemas } from 'graphql-tools';
import { GraphQLSchema } from 'graphql/type/schema';
import { Observable } from 'rxjs';
import { Environment } from '../core/environment';
import executableSchema from './schema';
import { remoteSchemas } from './remote-schema';
import { relevantLinkingResolvers, relevantLinkingSchemas } from './schema-linking';

class StitchedSchema {
	public outputSchema$: Observable<Promise<GraphQLSchema>>;
	private outputSchema: Promise<GraphQLSchema>;
	private localSchema: GraphQLSchema;

	constructor() {
		this.localSchema = executableSchema;
		this.outputSchema = Promise.resolve(this.localSchema);
		this.pollSchemas();
	}

	public getSchema() {
		return this.outputSchema;
	}

	public async createMergedSchema() {
		const remoteSchemasMap = await remoteSchemas.retreiveSchemas();
 
		return mergeSchemas({
			schemas: [this.localSchema, ...remoteSchemasMap.values(), ...relevantLinkingSchemas(remoteSchemasMap)],
			resolvers: relevantLinkingResolvers(remoteSchemasMap),
		});
	}

	private pollSchemas() {
		this.outputSchema$ = Observable.interval(Environment.getConfig().schemaStitching.pollRate).startWith(0)
			.map(() => this.createMergedSchema()).share();

		this.outputSchema$.subscribe(updatedSchema => {
			this.outputSchema = updatedSchema;
		});
	}
}

export const stitchedSchema = new StitchedSchema();
