export enum MicroServiceConstants {
    PLANNINGS_SERVICE = 'plannings-service',
    PRIORITIES_SERVICE = 'priorities-service',
    OC_SERVICE = 'oc-service',
    TA_SERVICE = 'ta-service',
    TAR_SERVICE = 'tar-service',
    USER_SERVICE = 'user-service',
    RE_SERVICE = 're-service',
    PL_SERVICE = 'pl-service',
    LAYERS_SERVICE = 'layers-service',
    DEMAND_SERVICE = 'demand-service'
}
