const cpx = require('cpx');

if (process.env.NODE_ENV === 'test') {
	cpx.copySync('src/api/**/*.{gql,graphql}', '.output/src/api');
	cpx.copySync('src/**/*.json', '.output/src');
	cpx.copySync('src/endpoints/graphiql/resources/*.{js,css}', '.output/endpoints/graphiql/resources')
	cpx.copySync('src/certs/*.{crt,pem}', '.output/src/certs');
} else {
	cpx.copySync('src/api/**/*.{gql,graphql}', 'dist/api');
	cpx.copySync('src/**/*.json', 'dist/');
	cpx.copySync('src/endpoints/graphiql/resources/*.{js,css}', 'dist/endpoints/graphiql/resources');
	cpx.copySync('src/certs/*.{crt,pem}', 'dist/certs');
}