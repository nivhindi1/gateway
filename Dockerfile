FROM docker-registry-default.app.osft/openshift/rhel-76-nodejs:10.14.2
WORKDIR /app
COPY . /app
ENV NODE_ENV int
CMD [ "node", "./dist/index.js" ]
