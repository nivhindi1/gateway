#!groovy

properties([buildDiscarder(logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '7', numToKeepStr: ''))])

node {

	OS_REGISTRY = 'docker-registry-default.app.osft'
	OS_MASTER_PASS = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJ3YXJyaW9yIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZWNyZXQubmFtZSI6ImJ1aWxkZXItdG9rZW4tdzZteHMiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC5uYW1lIjoiYnVpbGRlciIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50LnVpZCI6IjM3M2YzYmZmLWUzNDAtMTFlOC1hNTAxLTAwNTA1NmE4OTRkYSIsInN1YiI6InN5c3RlbTpzZXJ2aWNlYWNjb3VudDp3YXJyaW9yOmJ1aWxkZXIifQ.bvU1XLxdgHLceprfZs_EWV7h96oyH9YHWdlzineK6O73UHAlk0vQf6BkMbQAnGv4vl7wSSfNUcKkNG7_5emEy9z8kJ4Zq_tUSORP445JEfobSJC1jIH1IyX-3WgUGI6UtyVBPPqJYsVlFCGVQn7LY90WBlTm8sUxgU7y5i-P88rpUxIZzXumkCAq1ZAfJvUnATYbCTBdJ2oKPdVCuua-rYVRdYo0kgegIBYHgNlVUgyYL5cf2JSB09SDjjpB9kUMgVWmifBWkktfmCeua8Y7l5V8Oc0jopRQ45eSIkXiEJwsIukU5N9Oy0AKWK6ALhQcAAghTjnirrJfIbCV4oSFaw'
	OS_REGISTRY_USER = 'serviceaccount'
	OS_PROJ_NAME = 'warrior'
	IMAGE_NAME = 'gateway'
  
    stage("Clean directory") {
          deleteDir()
  	}
    
    stage('Checkout') {	
		checkout scm
	}
	
    stage('npm install') {	
        sh "npm install"
    }
 
    stage('npm run build') {	
       sh "npm run build"
    }
  
    try {
      stage('Sonar analysis') {
         allJob = JOB_NAME.tokenize('/') as String[]
         withSonarQubeEnv('Galileo Sonar') {
           sh "/software/sonar-scanner-3.3.0.1492-linux/bin/sonar-scanner -Dsonar.sources=src -Dsonar.exclusions=src/endpoints/graphiql/** -Dsonar.projectName='Galileo ${env.JOB_NAME.replaceAll("%2F","_").replaceAll(" ","_")}' -Dsonar.projectKey='iaf.ofek.galileo.${allJob[0].replaceAll("/","_").replaceAll(" ","_")}.${allJob[1].replaceAll("/","_").replaceAll(" ","_")}:${env.BRANCH_NAME.replaceAll("/","_").replaceAll(" ","_")}'"
         }
       }
    } catch(err) {
       echo "Sonar analysis failed. Continuing to the next step"
    }   	
  
    stage('Docker build and push') {
        echo "Branch Name : ${env.BRANCH_NAME}" 
        if (env.BRANCH_NAME == 'master') {    
		   sh "docker login -p ${OS_MASTER_PASS} -u ${OS_REGISTRY_USER} ${OS_REGISTRY}"
		   def customImage = docker.build("${OS_REGISTRY}/${OS_PROJ_NAME}/${IMAGE_NAME}:${env.BUILD_ID}")
           customImage.push()
           customImage.push('latest')
        }
    }
  
  
	stage('Remove images from agents') {	
        if (env.BRANCH_NAME == 'master') { 
		     sh "docker rmi ${OS_REGISTRY}/${OS_PROJ_NAME}/${IMAGE_NAME}:${env.BUILD_ID}"
             sh "docker rmi ${OS_REGISTRY}/${OS_PROJ_NAME}/${IMAGE_NAME}:latest"
             sh "docker logout ${OS_REGISTRY}"
        }
    }

    stage("Clean directory") {
          deleteDir()
  	}

}
